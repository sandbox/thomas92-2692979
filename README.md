Book outline - Drupal 7 module
==============================

Install
-------

1. Copy module into `sites/all/modules`
2. Navigate to site adminstration **Administration** > **Modules** (`[your page]/admin/modules`)
3. Turn on module

Dependency
----------
Only Drupal 7 _Book_ module.

Use
---
Navigate to `[your page]/book-outline`.